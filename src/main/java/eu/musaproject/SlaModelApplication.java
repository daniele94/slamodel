package eu.musaproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
//@ComponentScan(basePackages="it.southengineering.controller, it.southengineering.application, it.southengineering.helper")
@EntityScan(basePackages="eu.musaproject.model")
//@EnableJpaRepositories(basePackages="it.southengineering.repository")
public class SlaModelApplication {

	public static void main(String[] args) {
		SpringApplication.run(SlaModelApplication.class, args);
	}
}
