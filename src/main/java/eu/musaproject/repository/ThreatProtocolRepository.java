package eu.musaproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.musaproject.model.Protocol;
import eu.musaproject.model.ThreatProtocol;

public interface ThreatProtocolRepository extends JpaRepository<ThreatProtocol, Long> {
	
	@Query(value = "SELECT * FROM threatprotocol", nativeQuery = true )
	List<Protocol> getProtocols();

}
