package eu.musaproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.model.Protocol;


public interface protocolsRepository extends JpaRepository<Protocol, Long> 
{
	Protocol findById(long l);
	Protocol findByprotocol(String name); //findBy"AttributeName"


}
