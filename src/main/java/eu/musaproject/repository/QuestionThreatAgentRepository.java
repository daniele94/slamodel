package eu.musaproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.musaproject.model.QuestionControl;
import eu.musaproject.model.QuestionThreatAgent;

public interface QuestionThreatAgentRepository extends JpaRepository<QuestionThreatAgent, Long> {

	
	@Query(value = "SELECT * FROM question_threat_agents", nativeQuery = true )
	List<String> getComponentTypes();
}
