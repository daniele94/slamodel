package eu.musaproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.model.ThreatAgentCategory;


public interface ThreatAgentCategoryRepository extends JpaRepository<ThreatAgentCategory, Long> 
{
	ThreatAgentCategory findById(long l);
	ThreatAgentCategory findByname(String name); 


}
