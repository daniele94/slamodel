package eu.musaproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.model.Stride;

public interface StrideRepository extends JpaRepository<Stride, Long> {
	
	Stride findByName(String name);

}
