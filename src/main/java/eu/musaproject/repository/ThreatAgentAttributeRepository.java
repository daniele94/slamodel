package eu.musaproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.model.ThreatAgentAttribute;


public interface ThreatAgentAttributeRepository extends JpaRepository<ThreatAgentAttribute, Long> 
{
	ThreatAgentAttribute findById(long l);
	ThreatAgentAttribute findByscoreLabel(String name); 


}
