package eu.musaproject.utility;

import java.io.FileWriter;
import java.io.IOException;

public class Utilities {

	public static void writeFile(String message) throws IOException {
		FileWriter fw = new FileWriter("out.txt", true);
		fw.write(message+"\n");
		fw.close();
	}
}
