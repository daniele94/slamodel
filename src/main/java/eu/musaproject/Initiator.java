package eu.musaproject;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import javax.persistence.NonUniqueResultException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.musaproject.model.ComponentType;
import eu.musaproject.model.Control;
import eu.musaproject.model.Metric;
import eu.musaproject.model.Protocol;
import eu.musaproject.model.QuestionControl;
import eu.musaproject.model.QuestionThreat;
import eu.musaproject.model.QuestionThreatAgent;
import eu.musaproject.model.Role;
import eu.musaproject.model.Stride;
import eu.musaproject.model.Ta_has_attribute;
import eu.musaproject.model.Threat;
import eu.musaproject.model.ThreatAgentAttribute;
import eu.musaproject.model.ThreatAgentCategory;
import eu.musaproject.model.ThreatProtocol;
import eu.musaproject.repository.*;

@Component
public class Initiator  implements ApplicationRunner {
	public static final Logger logger = LoggerFactory.getLogger(Initiator.class);

	private ThreatRepository threatRepository;
	private QuestionThreatRepository questionThreatRepository;
	private StrideRepository strideRepository;
	private ControlRepository controlRepository;
	private MetricRepository metricRepository;
	private ComponentTypeRepository componentTypeRepository;
	private QuestionControlRepository questionControlRepository;
	private protocolsRepository protocolsRepository;
	private roleRepository roleRepository;
	private ThreatProtocolRepository threatprotocolRepository;
	private ThreatAgentCategoryRepository threatagentcategoryRepository;
	private ThreatAgentAttributeRepository threatagentattributeRepository;
	private ThreatAgentHasAttributeRepository threatagenthasattributeRepository;
	private QuestionThreatAgentRepository threatagentquestionrepository;


	

	@Value(value = "classpath:threats_questions.csv")
	private Resource threatsQuestionsCsv;

	@Value(value = "classpath:strides.csv")
	private Resource stridesCsv;

	@Value(value = "classpath:controls.csv")
	private Resource controlsCsv;

	@Value(value = "classpath:controls_nist_v2.xml")
	private Resource controlsXml;

	@Value(value = "classpath:metrics_v3.csv")
	private Resource metricsCsv;
	
	@Value(value = "classpath:threatprotocol.csv")
	private Resource ThreatProtocolCsv;


	@Value(value = "classpath:threats.csv")
	private Resource threatsCsv;

	@Value(value = "classpath:component_types.csv")
	private Resource componentTypesCsv;

	@Value(value = "classpath:metric_controls.csv")
	private Resource metricControlsCsv;

	@Value(value = "classpath:threat_strides_controls_questions.csv")
	private Resource threatStridesControlsQuestions;

	@Value(value = "classpath:threat_component_types.csv")
	private Resource threatComponentTypes;

	@Value(value = "classpath:controls_questions.csv")
	private Resource controlsQuestionsCsv;
	
	@Value(value = "classpath:protocols.csv")
	private Resource protocolsCsv;
	
	@Value(value = "classpath:threatprotocol.csv")
	private Resource threatprotocolCsv;
	
	@Value(value = "classpath:roles.csv")
	private Resource rolesCsv;
	
	@Value(value = "classpath:threat_agent_categories.csv")
	private Resource threatagentCategoryCsv;
	
	@Value(value = "classpath:threat_agent_attributes.csv")
	private Resource threatagentAttributesCsv;
	
	@Value(value = "classpath:threat_agent_has_attributes.csv")
	private Resource threatagenthasAttributesCsv;
	
	@Value(value = "classpath:threat_agent_questions.csv")
	private Resource threatagentquestionsCsv;

	@Value(value = "${update.database}")
	private Boolean isDatabaseUpdate;


	@Autowired
	public Initiator(QuestionThreatRepository questionRepository, StrideRepository strideRepository, 
			ControlRepository controlRepository, MetricRepository metricRepository, 
			ThreatRepository threatRepository, ComponentTypeRepository componentTypeRepository,
			QuestionControlRepository questionControlRepository,protocolsRepository protocolsRepository,roleRepository roleRepository,
			ThreatProtocolRepository threatprotocolRepository,ThreatAgentCategoryRepository threatagentcategoryRepository,
			ThreatAgentAttributeRepository threatagentattributeRepository,ThreatAgentHasAttributeRepository threatagenthasattributeRepository,
			QuestionThreatAgentRepository threatagentquestionrepository) {
		this.questionThreatRepository = questionRepository;
		this.strideRepository = strideRepository;
		this.controlRepository = controlRepository;
		this.metricRepository = metricRepository;
		this.threatRepository = threatRepository;
		this.componentTypeRepository = componentTypeRepository;
		this.questionControlRepository = questionControlRepository;
		this.protocolsRepository=protocolsRepository;
		this.roleRepository=roleRepository;
		this.threatprotocolRepository=threatprotocolRepository;
		this.threatagentcategoryRepository=threatagentcategoryRepository;
		this.threatagentattributeRepository=threatagentattributeRepository;
		this.threatagenthasattributeRepository=threatagenthasattributeRepository;
		this.threatagentquestionrepository=threatagentquestionrepository;
	}

	public void run(ApplicationArguments args) {
		//		editControlsXml();
		if(questionThreatRepository.findAll().size() == 0){
			initQuestions();
		}
		
		if(threatagentcategoryRepository.findAll().size() == 0){
			initThreatAgentCategory();
		}

		if(strideRepository.findAll().size() == 0){
			initStrides();
		}
		if(protocolsRepository.findAll().size() == 0){
			initProtocols();
		}

		if(controlRepository.findAll().size() == 0){
			initControls();
		}

		if(isDatabaseUpdate){
			updateMetrics();
		}else{
			if(metricRepository.findAll().size() == 0){
				initMetrics();
			}
		}

		if(threatRepository.findAll().size() == 0){
			initThreats();
		}

		if(componentTypeRepository.findAll().size() == 0){
			initComponentTypes();
			logger.info("ADD COMPONENT TYPES TO THREATS");
			addComponentTypesToThreats();
		}

		if(questionControlRepository.findAll().size() == 0){
			initQuestionsControl();
		}
		if(roleRepository.findAll().size() == 0){
			initRoles();
		}
		if(threatprotocolRepository.findAll().size() == 0)
		{
			initThreatProtocol();
		}
		if(threatagentattributeRepository.findAll().size()==0)
		{
			initThreatAgentAttribute();
		}
		if(threatagenthasattributeRepository.findAll().size()==0)
		{
		initTA_has_Attribute();
		}
		if(threatagentquestionrepository.findAll().size()==0)
		{
			initThreatAgentQuestions();
		}

		//Filling Associations 
		logger.info("Create Associations tables");

		logger.info("ADD METRICS TO CONTROLS");
		if(isDatabaseUpdate){
			updateMetricsToControls();
		}else{
			if(controlRepository.getControlMetrics().size() == 0){
				addMetricsToControls();
			}
		}


		logger.info("ADD STRIDES CONTROLS AND QUESTIONS TO THREATS");
		if(threatRepository.getThreatStrides().size() == 0 || isDatabaseUpdate){
			addStridesControlsAndQuestionsToThreats();
		}

		logger.info("ADD QUESTIONS TO CONTROLS");
		if(controlRepository.getControlQuestions().size() == 0){
			addQuestionsToControls();
		}
		
		if(threatprotocolRepository.findAll().size() == 0){
			initThreatProtocol();
		}


		logger.info("ADD COMPONENT TYPES TO QUESTIONS CONTROLS");
		if(questionControlRepository.getComponentTypes().size() == 0){
			addComponentTypesToQuestionsControl();
			logger.info("ADD DEFAULT QUESTIONS TO CONTROLS");
			addDefaultQuestionsToControls();
		}


	}

	private void initQuestions(){
		questionThreatRepository.deleteAll();

		try (Scanner scanner = new Scanner(threatsQuestionsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				QuestionThreat question = new QuestionThreat();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				question.setQuestionId(values[0]);
				question.setDescription(values[1]);
				questionThreatRepository.save(question);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initThreatAgentCategory(){
		threatagentcategoryRepository.deleteAll();

		try (Scanner scanner = new Scanner(threatagentCategoryCsv.getFile())) {
			while (scanner.hasNextLine()) {
				ThreatAgentCategory tac=new ThreatAgentCategory();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				tac.setName(values[0]);
				threatagentcategoryRepository.save(tac);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	private void initThreatProtocol(){
		threatprotocolRepository.deleteAll();
		
		try (Scanner scanner = new Scanner(ThreatProtocolCsv.getFile())) {
			while (scanner.hasNextLine()) {
				ThreatProtocol threatprotocol= new ThreatProtocol();
				Threat t=new Threat();
				Protocol p=new Protocol();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				Long protocolId= Long.parseLong(values[1].substring(1));
				
				t=threatRepository.findByThreatId(values[0]);
				p=protocolsRepository.findById(protocolId);

				threatprotocol.setProtocol(p);
				threatprotocol.setThreat(t);
				threatprotocolRepository.save(threatprotocol);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	private void initTA_has_Attribute(){
		threatagenthasattributeRepository.deleteAll();
		
		try (Scanner scanner = new Scanner(threatagenthasAttributesCsv.getFile())) {
			while (scanner.hasNextLine()) {
				ThreatAgentCategory tac=new ThreatAgentCategory();
				ThreatAgentAttribute taa=new ThreatAgentAttribute();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				int count=0;
				//taha.setTa_category(tac);
				for(String value: values)
				{
					tac=threatagentcategoryRepository.findByname(values[0]);
					if(count!=0)
					{
						Ta_has_attribute taha= new Ta_has_attribute();

						taha.setTa_category(tac);
						String[] multiple_attribute = value.split(",");

						for(String single_attribute: multiple_attribute)
						{

							taa=threatagentattributeRepository.findByscoreLabel(single_attribute);		
							taha.setTaAttribute(taa);
							logger.info("Categoria "+taha.getTa_category().getName()+" Attributo "+ taha.getTaAttribute().getScoreLabel());
							threatagenthasattributeRepository.save(taha);

						}
					}
					count++;
				}
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

	private void initStrides(){
		strideRepository.deleteAll();

		try (Scanner scanner = new Scanner(stridesCsv.getFile())) {
			while (scanner.hasNextLine()) {
				Stride stride = new Stride();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				stride.setName(values[0]);
				stride.setDescription("");
				strideRepository.save(stride);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initProtocols(){
		protocolsRepository.deleteAll();

		try (Scanner scanner = new Scanner(protocolsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				Protocol protocol = new Protocol();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				protocol.setProtocol(values[0]);
				protocolsRepository.save(protocol);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initRoles(){
		roleRepository.deleteAll();
		try (Scanner scanner = new Scanner(rolesCsv.getFile())) 
		{
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				String roleString=values[0];
				int count=0;
				Protocol protocol=null;

				for(String value: values)
				{
					Role role=new Role();

						protocol=protocolsRepository.findByprotocol(roleString);
						role.setProtocol(protocol);
					
					if(count!=0)
					{
						role.setrole(value);
					}
					
					if(role.getProtocol()!=null && role.getrole()!=null)
						roleRepository.save(role);
					count++;
				}
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	private void initThreatAgentAttribute(){
		threatagentattributeRepository.deleteAll();
		try (Scanner scanner = new Scanner(threatagentAttributesCsv.getFile())) 
		{
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");

				int count=0;
				

				for(String value: values)
				{
					ThreatAgentAttribute taA=new ThreatAgentAttribute();
					taA.setAttribute(values[0]);
					
					if(count!=0)
					{
						taA.setScoreLabel(value);
						taA.setScore(count);
						threatagentattributeRepository.save(taA);

					}
					
					count++;
				}
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	

	private void initControls(){
		controlRepository.deleteAll();

		//Get controls from xml
		try{
			DocumentBuilderFactory dbFactory 
			= DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(controlsXml.getFile());
			doc.getDocumentElement().normalize();
			NodeList securityControls = doc.getElementsByTagName("specs:security_control");

			for(int i = 0; i < securityControls.getLength(); i++){
				Node securityControl = securityControls.item(i);

				if (securityControl.getNodeType() == Node.ELEMENT_NODE) {
					Control control = new Control();
					Element eElement = (Element) securityControl;
					control.setControlId(eElement.getAttribute("id"));
					control.setControlName(eElement.getAttribute("name"));
					control.setFamilyId(eElement.getAttribute("nist:control_family_id"));
					control.setFamilyName(eElement.getAttribute("nist:control_family_name"));
					control.setControl(Integer.valueOf(eElement.getAttribute("nist:security_control")));
					control.setEnhancement(Integer.valueOf(eElement.getAttribute("nist:control_enhancement")));
					control.setControlDescription(eElement.getElementsByTagName("specs:control_description").item(0).getTextContent());
					control.setRisk(eElement.getAttribute("nist:risk"));
					controlRepository.save(control);
				}
			}


		}catch (Exception e) {
			e.printStackTrace();
		}

		//Get controls from csv
		/*
		try (Scanner scanner = new Scanner(controlsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				Control control = new Control();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				control.setControlId(values[0]);
				control.setControlName(values[1]);
				control.setFamilyId(values[2]);
				control.setFamilyName(values[3]);
				control.setControl(Integer.valueOf(values[4]));
				control.setEnhancement(Integer.valueOf(values[5]));
				control.setControlDescription(values[6]);
				if(values[7] != null && !values[7].equals("NULL")){
					control.setMinRisk(Integer.valueOf(values[7]));
				}else{
					control.setMinRisk(0);
				}
				controlRepository.save(control);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 */
	}

	//	private void editControlsXml(){
	//		List<Control> csvControls = new ArrayList<Control>();
	//		try (Scanner scanner = new Scanner(controlsCsv.getFile())) {
	//			while (scanner.hasNextLine()) {
	//				Control control = new Control();
	//				String line = scanner.nextLine();
	//				String[] values = line.split(";");
	//				control.setControlId(values[0]);
	//				control.setControlName(values[1]);
	//				control.setFamilyId(values[2]);
	//				control.setFamilyName(values[3]);
	//				control.setControl(Integer.valueOf(values[4]));
	//				control.setEnhancement(Integer.valueOf(values[5]));
	//				control.setControlDescription(values[6]);
	////				if(values[7] != null && !values[7].equals("NULL")){
	////					control.setMinRisk(Integer.valueOf(values[7]));
	////				}else{
	////					control.setMinRisk(0);
	////				}
	//				control.setRisk(values[8]);
	//				csvControls.add(control);
	//			}
	//			scanner.close();
	//		} catch (IOException e) {
	//			e.printStackTrace();
	//		}
	//		
	//		try{
	//			DocumentBuilderFactory dbFactory 
	//			= DocumentBuilderFactory.newInstance();
	//			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	//			Document doc = dBuilder.parse(controlsXml.getFile());
	//			doc.getDocumentElement().normalize();
	//			NodeList securityControls = doc.getElementsByTagName("specs:security_control");
	//
	//			for(int i = 0; i < securityControls.getLength(); i++){
	//				Node securityControl = securityControls.item(i);
	//
	//				if (securityControl.getNodeType() == Node.ELEMENT_NODE) {
	//					Element eElement = (Element) securityControl;
	//					for(Control csvControl : csvControls){
	//						eElement.setAttribute("nist:risk", csvControl.getRisk());
	//					}
	//				}
	//			}
	//
	//			// write the content into xml file
	//            TransformerFactory transformerFactory = TransformerFactory
	//                    .newInstance();
	//            Transformer transformer = transformerFactory.newTransformer();
	//            DOMSource source = new DOMSource(doc);
	//            StreamResult result = new StreamResult(new File("/Users/Pasquale/Desktop/controls_xml.xml"));
	//            transformer.transform(source, result);
	//
	//		}catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}

	private void initMetrics(){
		metricRepository.deleteAll();

		try (Scanner scanner = new Scanner(metricsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				Metric metric = new Metric();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				metric.setMetricId(values[0]);
				metric.setMetricName(values[1]);
				metric.setApplicability(values[3]);
				metric.setMetricDescription(values[4]);
				metric.setFormula(values[5]);
				metric.setValueRange(values[6]);
				metric.setUnit(values[7]);
				metric.setValueDescription(values[8]);
				metric.setDefaultValue(values[9]);
				metric.setOperator(values[10]);
				metric.setSource(values[11]);
				metricRepository.save(metric);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void updateMetrics(){
		try (Scanner scanner = new Scanner(metricsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");

				Metric metric = metricRepository.findByMetricId(values[0]);
				if(metric == null){
					metric = new Metric();
				}

				metric.setMetricId(values[0]);
				metric.setMetricName(values[1]);
				metric.setApplicability(values[3]);
				metric.setMetricDescription(values[4]);
				metric.setFormula(values[5]);
				metric.setValueRange(values[6]);
				metric.setUnit(values[7]);
				metric.setValueDescription(values[8]);
				metric.setDefaultValue(values[9]);
				metric.setOperator(values[10]);
				metric.setSource(values[11]);
				metricRepository.save(metric);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initThreats(){
		threatRepository.deleteAll();

		try (Scanner scanner = new Scanner(threatsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				Threat threat = new Threat();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				threat.setThreatId(values[0]);
				threat.setThreatName(values[1]);
				threat.setThreatDescription(values[2]);
				threat.setCountermeasures(values[3]);
				threat.setSkillLevel(Integer.valueOf(values[4]));
				threat.setMotive(Integer.valueOf(values[5]));
				threat.setOpportunity(Integer.valueOf(values[6]));
				threat.setSize(Integer.valueOf(values[7]));
				threat.setEaseOfDiscovery(Integer.valueOf(values[8]));
				threat.setEaseOfExploit(Integer.valueOf(values[9]));
				threat.setAwareness(Integer.valueOf(values[10]));
				threat.setIntrusionDetection(Integer.valueOf(values[11]));
				threat.setLossOfConfidentiality(Integer.valueOf(values[12]));
				threat.setLossOfIntegrity(Integer.valueOf(values[13]));
				threat.setLossOfAvailability(Integer.valueOf(values[14]));
				threat.setLossOfAccountability(Integer.valueOf(values[15]));
				threat.setSource(values[16]);
				threatRepository.save(threat);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initComponentTypes(){
		componentTypeRepository.deleteAll();

		try (Scanner scanner = new Scanner(componentTypesCsv.getFile())) {
			while (scanner.hasNextLine()) {
				ComponentType componentType = new ComponentType();
				String line = scanner.nextLine();
				String[] values = line.split(";");
				componentType.setName(values[0]);
				componentTypeRepository.save(componentType);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initQuestionsControl(){
		questionControlRepository.deleteAll();

		try (Scanner scanner = new Scanner(controlsQuestionsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				QuestionControl question = new QuestionControl();
				String line = scanner.nextLine();
				String[] values = line.split(";"); 
				question.setSourceName(values[0]);
				question.setSourceId(values[1]);
				question.setDescription(values[2]);
				questionControlRepository.save(question);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void initThreatAgentQuestions(){
		threatagentquestionrepository.deleteAll();

		try (Scanner scanner = new Scanner(threatagentquestionsCsv.getFile())) {
			int count=1;
			while (scanner.hasNextLine()) 
			{
				QuestionThreatAgent qta = new QuestionThreatAgent();
				String line = scanner.nextLine();
				String[] values = line.split(";"); 
				qta.setQuestion(values[0]);
				qta.setQuestionId("Q"+count);
				threatagentquestionrepository.save(qta);
				count++;
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}

	

	//Relation tables methods
	private void addMetricsToControls(){

		try (Scanner scanner = new Scanner(metricControlsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				Metric metric = metricRepository.findByMetricId(values[0]);
				if(metric != null){
					if(values.length > 1){
						String[] controlsId = values[1].split(",");

						for(String controlId : controlsId){
							Control control = controlRepository.findByControlId(controlId);

							if(control != null){
								control.addMetric(metric);
								controlRepository.save(control);
							}else{
								System.err.println("CONTROL OF METRIC WITH ID:"+values[0]+" NOT FOUND!!!! CID:"+controlId);
							}

						}
					}
				}else{
					System.err.println("METRIC NOT FOUND!!!! "+values[0]);
				}
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void updateMetricsToControls(){

		try (Scanner scanner = new Scanner(metricControlsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				Metric metric = metricRepository.findByMetricId(values[0]);
				if(metric != null){
					if(values.length > 1){
						String[] controlsId = values[1].split(",");

						for(String controlId : controlsId){
							Control control = controlRepository.findByControlId(controlId);

							boolean metricFound = false;
							if(control != null){
								for(Metric metricNow : control.getMetrics()){
									if(metricNow.getMetricId().equals(metric.getMetricId())){
										metricFound = true;
										break;
									}
								}
							}
							if(control != null && !metricFound){
								control.addMetric(metric);
								controlRepository.save(control);
							}else{
								System.err.println("CONTROL OF METRIC WITH ID:"+values[0]+" NOT FOUND!!!! CID:"+controlId);
							}

						}
					}
				}else{
					System.err.println("METRIC NOT FOUND!!!! "+values[0]);
				}
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void addStridesControlsAndQuestionsToThreats(){

		try (Scanner scanner = new Scanner(threatStridesControlsQuestions.getFile())) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				Threat threat = threatRepository.findByThreatId(values[0]);
				if(threat != null){
					//					System.out.println("THREAT FOUND!");

					if(values.length > 1){

						//STRIDE
						threat.resetStrides();
						String[] stridesName = values[1].split(",");
						for(String strideName : stridesName){
							Stride stride = strideRepository.findByName(strideName);
							if(stride != null){
								threat.addStride(stride);
							}else{
								System.err.println("STRIDE NOT FOUND!!!! "+strideName);
							}
						}

						//CONTROLS
						threat.resetSuggestedControls();
						String[] controlsId = values[2].split(",");
						for(String controlId : controlsId){
							Control control = controlRepository.findByControlId(controlId);
							if(control != null){
								threat.addSuggestedControl(control);
							}else{
								System.err.println("CONTROL FOR THREAT WITH ID: "+threat.getThreatId()+" NOT FOUND!!!! CID:"+controlId);
							}
						}

						//QUESTIONS
						threat.resetQuestions();
						String[] questionsId = values[3].split(",");
						for(String questionId : questionsId){
							QuestionThreat question = questionThreatRepository.findByQuestionId(questionId);
							if(question != null){
								threat.addQuestion(question);
							}else{
								System.err.println("QUESTION NOT FOUND!!!! "+questionId);
							}
						}

						threatRepository.save(threat);

					}
				}else{
					System.err.println("THREAT NOT FOUND!!!! "+values[0]);
				}
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void addComponentTypesToThreats(){

		try (Scanner scanner = new Scanner(threatComponentTypes.getFile())) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				Threat threat = threatRepository.findByThreatId(values[0]);
				if(threat != null){
					if(values.length > 1){
						String[] componentTypesName = values[1].split(",");

						for(String componentTypeName : componentTypesName){
							ComponentType componentType = componentTypeRepository.findByName(componentTypeName);

							if(componentType != null){
								threat.addComponentType(componentType);

							}else{
								System.err.println("COMPONENT TYPE NOT FOUND!!!! "+componentTypeName);
							}
						}
						threatRepository.save(threat);
					}
				}else{
					System.err.println("THREAT NOT FOUND!!!! "+values[0]);
				}
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void addQuestionsToControls(){

		try (Scanner scanner = new Scanner(controlsQuestionsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				QuestionControl questionControl = questionControlRepository.findBySourceId(values[1]);
				if(questionControl != null){
					if(values.length > 1){
						String[] controlsId = values[3].split(",");

						for(String controlId : controlsId){
							Control control = controlRepository.findByControlId(controlId);

							if(control != null){
								control.addQuestionControl(questionControl);
								controlRepository.save(control);
							}else{
								System.err.println("CONTROL FOR QUESTION WITH ID:"+values[1]+" NOT FOUND!!!! CID:"+controlId);
							}

						}
					}
				}else{
					System.err.println("QUESTION NOT FOUND!!!! "+values[1]);
				}
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private void addComponentTypesToQuestionsControl(){

		try (Scanner scanner = new Scanner(controlsQuestionsCsv.getFile())) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				QuestionControl questionControl = questionControlRepository.findBySourceId(values[1]);
				if(questionControl != null){
					if(values.length > 1){
						String[] componentsTypeName = values[4].split(",");

						for(String componentTypeName : componentsTypeName){
							ComponentType componentType = componentTypeRepository.findByName(componentTypeName);

							if(componentType != null){
								questionControl.addComponentType(componentType);
							}else{
								System.err.println("COMPONENT TYPE NOT FOUND!!!! "+componentTypeName);
							}
						}
						questionControlRepository.save(questionControl);
					}
				}else{
					System.err.println("QUESTION NOT FOUND!!!! "+values[1]);
				}
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void addDefaultQuestionsToControls(){
		List<Control> allControls = controlRepository.findAll();
		for(Control control : allControls){	
			//			if(control.getQuestionControl().size() == 0){
			QuestionControl questionControl = new QuestionControl();
			questionControl.setSourceName("NIST SP800-53");
			questionControl.setSourceId(control.getControlId());
			questionControl.setDescription(control.getControlDescription());
			questionControl = questionControlRepository.save(questionControl);

			List<ComponentType> componentTypes = componentTypeRepository.findAll();
			for(ComponentType componentType: componentTypes){
				questionControl.addComponentType(componentType);
			}
			questionControl = questionControlRepository.save(questionControl);

			control.addQuestionControl(questionControl);
			controlRepository.save(control);
			//			}
		}
	}
}