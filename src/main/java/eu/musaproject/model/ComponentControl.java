package eu.musaproject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "component_controls")
public class ComponentControl {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "component_id")
	private Component component;
	
	@OneToOne
	@JoinColumn(name = "controlId")
	private Control control;

	@Column(name="confirmed")
	private Boolean confirmed;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "componentControl")
	private Set<ComponentControlMetric> controlMetrics;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "componentControl")
	private Set<ComponentControlQuestion> controlQuestions;
	
	public ComponentControl(){
		controlMetrics = new HashSet<ComponentControlMetric>();
		controlQuestions = new HashSet<ComponentControlQuestion>();
	}
	
	public ComponentControl(Control control){
		setControl(control);
		setConfirmed(true);
		controlMetrics = new HashSet<ComponentControlMetric>();
		controlQuestions = new HashSet<ComponentControlQuestion>();
		
		System.out.println("Setup Component control: "+control.getControlName());
		
		System.out.println("Number of metrics associated to control: "+control.getMetrics().size());
		for (Metric controlMetric : control.getMetrics()){
			System.out.println("Adding metric: "+controlMetric.getMetricName());
			controlMetrics.add(new ComponentControlMetric(controlMetric, this));
		}
		
		System.out.println("Number of question associated to control: "+control.getQuestionControl().size());
		for (QuestionControl questionControl : control.getQuestionControl()){
			System.out.println("Adding question: "+questionControl.getSourceName());
			controlQuestions.add(new ComponentControlQuestion(questionControl, this));
		}
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}
	
	public Control getControl() {
		return control;
	}

	public void setControl(Control control) {
		this.control = control;
	}	

	public Long getId() {
		return id;
	}
	
	public Boolean getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}
	
	public Set<ComponentControlMetric> getControlMetrics() {
		return controlMetrics;
	}

	public void addThreatControlMetric(ComponentControlMetric controlMetric) {
		getControlMetrics().add(controlMetric);
		if(controlMetric.getComponentControl() != this){
			controlMetric.setComponentControl(this);
		}
	}
	
	public Set<ComponentControlQuestion> getControlQuestions() {
		return controlQuestions;
	}

	public void addControlQuestion(ComponentControlQuestion controlQuestion) {
		getControlQuestions().add(controlQuestion);
		if(controlQuestion.getComponentControl() != this){
			controlQuestion.setComponentControl(this);
		}
	}
}
