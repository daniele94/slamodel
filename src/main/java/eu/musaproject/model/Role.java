package eu.musaproject.model;



import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name = "role")
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	
	@Column(name="role")
	private String role;
	

	@ManyToOne
	@JoinColumn(name = "protocol")
	private Protocol protocol;

	public Role()
	{}



	public Protocol getProtocol() {
		return protocol;
	}



	public void setProtocol(Protocol protocol) {
		this.protocol=protocol;
	}



	public void setrole(String t) {
		this.role = t;
	}

	public String getrole() {
		return role;
	}
	

	

	
	

	
}
