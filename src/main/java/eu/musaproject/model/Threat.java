package eu.musaproject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "threats")
public class Threat {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "threat_id", unique = true)
	private String threatId;
	
	@Column(name = "threat_name")
	private String threatName;
	
	@Column(name = "threat_description")
	@Type(type="text")
	private String threatDescription;
	
	@Column(name = "countermeasures")
	@Type(type="text")
	private String countermeasures;

	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Stride> strides;
	
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Control> suggestedControls;

	@Column(name = "source")
	private String source;
	
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<QuestionThreat> questions;

	@Column(name = "skill_level")
	@Min(0) @Max(9)
	private Integer skillLevel;
	
	@Column(name = "motive")
	@Min(0) @Max(9)
	private Integer motive;
	
	@Column(name = "opportunity")
	@Min(0) @Max(9)
	private Integer opportunity;
	
	@Column(name = "size")
	@Min(0) @Max(9)
	private Integer size;
	
	@Column(name = "ease_of_discovery")
	@Min(0) @Max(9)
	private Integer easeOfDiscovery;
	
	@Column(name = "ease_of_exploit")
	@Min(0) @Max(9)
	private Integer easeOfExploit;
	
	@Column(name = "awareness")
	@Min(0) @Max(9)
	private Integer awareness;
	
	@Column(name = "intrusion_detection")
	@Min(0) @Max(9)
	private Integer intrusionDetection;
	
	@Column(name = "loss_of_confidentiality")
	@Min(0) @Max(9)
	private Integer lossOfConfidentiality;
	
	@Column(name = "loss_of_integrity")
	@Min(0) @Max(9)
	private Integer lossOfIntegrity;
	
	@Column(name = "loss_of_availability")
	@Min(0) @Max(9)
	private Integer lossOfAvailability;
	
	@Column(name = "loss_of_accountability")
	@Min(0) @Max(9)
	private Integer lossOfAccountability;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<ComponentType> componentTypes;
	
	public Threat(){
		strides = new HashSet<Stride>();
		suggestedControls = new HashSet<Control>();
		questions = new HashSet<QuestionThreat>();
		componentTypes = new HashSet<ComponentType>();
	}

	public String getThreatId() {
		return threatId;
	}

	public void setThreatId(String threatId) {
		this.threatId = threatId;
	}

	public String getThreatName() {
		return threatName;
	}

	public void setThreatName(String threatName) {
		this.threatName = threatName;
	}

	public String getThreatDescription() {
		return threatDescription;
	}

	public void setThreatDescription(String threatDescription) {
		this.threatDescription = threatDescription;
	}

	public void resetQuestions(){
		questions = new HashSet<QuestionThreat>();
	}
	
	public Set<QuestionThreat> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<QuestionThreat> questions) {
		this.questions = questions;
	}
	
	public void addQuestion(QuestionThreat question){
		getQuestions().add(question);
	}

	public void resetStrides(){
		strides = new HashSet<Stride>();
	}
	
	public Set<Stride> getStrides() {
		return strides;
	}

	public void setStrides(Set<Stride> strides) {
		this.strides = strides;
	}
	
	public void addStride(Stride stride) {
		getStrides().add(stride);
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Integer getSkillLevel() {
		return skillLevel;
	}

	public void setSkillLevel(Integer skillLevel) {
		this.skillLevel = skillLevel;
	}

	public Integer getMotive() {
		return motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public Integer getOpportunity() {
		return opportunity;
	}

	public void setOpportunity(Integer opportunity) {
		this.opportunity = opportunity;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getEaseOfDiscovery() {
		return easeOfDiscovery;
	}

	public void setEaseOfDiscovery(Integer easeOfDiscovery) {
		this.easeOfDiscovery = easeOfDiscovery;
	}

	public Integer getEaseOfExploit() {
		return easeOfExploit;
	}

	public void setEaseOfExploit(Integer easeOfExploit) {
		this.easeOfExploit = easeOfExploit;
	}

	public Integer getAwareness() {
		return awareness;
	}

	public void setAwareness(Integer awareness) {
		this.awareness = awareness;
	}

	public Integer getIntrusionDetection() {
		return intrusionDetection;
	}

	public void setIntrusionDetection(Integer intrusionDetection) {
		this.intrusionDetection = intrusionDetection;
	}

	public Integer getLossOfConfidentiality() {
		return lossOfConfidentiality;
	}

	public void setLossOfConfidentiality(Integer lossOfConfidentiality) {
		this.lossOfConfidentiality = lossOfConfidentiality;
	}

	public Integer getLossOfIntegrity() {
		return lossOfIntegrity;
	}

	public void setLossOfIntegrity(Integer lossOfIntegrity) {
		this.lossOfIntegrity = lossOfIntegrity;
	}

	public Integer getLossOfAvailability() {
		return lossOfAvailability;
	}

	public void setLossOfAvailability(Integer lossOfAvailability) {
		this.lossOfAvailability = lossOfAvailability;
	}

	public Integer getLossOfAccountability() {
		return lossOfAccountability;
	}

	public void setLossOfAccountability(Integer lossOfAccountability) {
		this.lossOfAccountability = lossOfAccountability;
	}

	public Long getId() {
		return id;
	}
	
	public void resetSuggestedControls(){
		suggestedControls = new HashSet<Control>();
	}
	
	public void addSuggestedControl(Control control) {
		getSuggestedControls().add(control);
	}
	
	public Set<Control> getSuggestedControls() {
		return suggestedControls;
	}

	public void setSuggestedControls(Set<Control> suggestedControls) {
		this.suggestedControls = suggestedControls;
	}
	
	public String getCountermeasures() {
		return countermeasures;
	}

	public void setCountermeasures(String countermeasures) {
		this.countermeasures = countermeasures;
	}
	
	public Set<ComponentType> getComponentTypes() {
		return componentTypes;
	}

	public void setComponentTypes(Set<ComponentType> componentTypes) {
		this.componentTypes = componentTypes;
	}
	
	public void addComponentType(ComponentType componentType){
		getComponentTypes().add(componentType);
	}
	
	public void resetComponentTypes(){
		componentTypes = new HashSet<ComponentType>();
	}
}
