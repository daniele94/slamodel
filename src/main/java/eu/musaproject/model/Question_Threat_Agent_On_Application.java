package eu.musaproject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import eu.musaproject.enumeration.ApplicationState;

@Entity
@Table(name = "question_threat_agent_on_application")
public class Question_Threat_Agent_On_Application {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	public QuestionThreatAgent getQta() {
		return qta;
	}

	public void setQta(QuestionThreatAgent qta) {
		this.qta = qta;
	}

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "question_threat_agent")
	private QuestionThreatAgent qta;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "application")
	private Application application;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
	
}
