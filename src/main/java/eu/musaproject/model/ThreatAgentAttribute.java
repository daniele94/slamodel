package eu.musaproject.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

@Entity
@Table(name = "threatagent_attributes")
public class ThreatAgentAttribute {	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "attribute")
	private String attribute;

	@Column(name = "scoreLabel")
	private String scoreLabel;
	
	public String getScoreLabel() {
		return scoreLabel;
	}

	public void setScoreLabel(String scoreLabel) {
		this.scoreLabel = scoreLabel;
	}

	@Column(name = "score")
	private int score;

	@Column(name = "Description")
	String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}






	
	
}
