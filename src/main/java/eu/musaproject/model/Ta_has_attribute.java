package eu.musaproject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import eu.musaproject.enumeration.ApplicationState;

@Entity
@Table(name = "ta_has_attribute")
public class Ta_has_attribute {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "ta_category")
	private ThreatAgentCategory ta_category;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "taAttribute")
	private ThreatAgentAttribute taAttribute;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ThreatAgentCategory getTa_category() {
		return ta_category;
	}

	public void setTa_category(ThreatAgentCategory ta_category) {
		this.ta_category = ta_category;
	}

	public ThreatAgentAttribute getTaAttribute() {
		return taAttribute;
	}

	public void setTaAttribute(ThreatAgentAttribute taAttribute) {
		this.taAttribute = taAttribute;
	}
	
	
	
}
